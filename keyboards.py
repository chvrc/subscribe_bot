from aiogram.types import (InlineKeyboardMarkup, InlineKeyboardButton, 
                            ReplyKeyboardMarkup, KeyboardButton)


async def kb_admin():
    bt1 = InlineKeyboardButton('Канал', callback_data='chat')
    bt2 = InlineKeyboardButton('Подписка', callback_data='option')
    bt_back = InlineKeyboardButton('Выйти из панели', callback_data='exit')
    kb = InlineKeyboardMarkup()
    kb.add(bt1, bt2).add(bt_back)
    return kb


async def kb_back():
    bt_back = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt_back)
    return kb


async def kb_pay_yookassa(pay_url):
    bt1 = InlineKeyboardButton('Оплатить (ЮKassa)', url=pay_url)
    kb = InlineKeyboardMarkup()
    kb.add(bt1)
    return kb


async def kb_success_pay(url):
    bt1 = InlineKeyboardButton('Перейти на канал', url=url)
    kb = InlineKeyboardMarkup()
    kb.add(bt1)
    return kb


async def kb_edit_chat():
    bt1 = InlineKeyboardButton('Название', callback_data='edit_chat_name')
    bt2 = InlineKeyboardButton('ID', callback_data='edit_chat_id')
    bt_back = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt1, bt2).add(bt_back)
    return kb


async def kb_edit_option():
    bt1 = InlineKeyboardButton('Стоимость', callback_data='edit_option_price')
    bt2 = InlineKeyboardButton('Срок', callback_data='edit_option_time')
    bt_back = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt1, bt2).add(bt_back)
    return kb


async def kb_desc():
    bt1 = InlineKeyboardButton('Описание клуба', callback_data='desc')
    bt2 = InlineKeyboardButton('Оплатить клуб', callback_data='email')
    kb = InlineKeyboardMarkup()
    kb.add(bt1).add(bt2)
    return kb


async def kb_email(email):
    bt1 = KeyboardButton(email)
    kb = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    kb.add(bt1)
    return kb


async def kb_cancel_sub():
    bt1 = InlineKeyboardButton('Отменить подписку', callback_data='cancel_sub')
    kb = InlineKeyboardMarkup()
    kb.add(bt1)
    return kb