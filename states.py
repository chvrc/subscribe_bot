from aiogram.dispatcher.filters.state import State, StatesGroup

class Admin(StatesGroup):
    start = State()
    edit_chat_name = State()
    edit_chat_id = State()
    edit_option_price = State()
    edit_option_time = State()


class MainMenu(StatesGroup):
    start = State()
    email = State()
    desc = State()