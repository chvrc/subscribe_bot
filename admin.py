from aiogram.dispatcher.storage import FSMContext
from aiogram.dispatcher.filters import Command
from db.chats import edit_chat_id, edit_chat_name, get_chat
from db.sub_options import edit_sub_option_price, edit_sub_option_time, get_sub_option
from keyboards import kb_admin, kb_edit_chat, kb_edit_option
from load_bot import ADMIN_ID, dp
from aiogram import types
from states import Admin


@dp.message_handler(Command('admin'), state='*', user_id=ADMIN_ID)
async def cmd_admin(message: types.Message, state: FSMContext):
    chat = get_chat(1)
    option = get_sub_option(1)
    ans = '<u>Канал</u>\n' \
        '➖Название: <b>{}</b>\n' \
        '➖ID: <b>{}</b>\n' \
        '<u>Подписка</u>\n' \
        '➖Стоимость: <b>{} руб.</b>\n' \
        '➖Срок: <b>{} дней</b>'.format(
        chat[1], str(chat[2]).replace('-100', ''),
        option[1], option[2]
    )
    kb = await kb_admin()
    await message.answer(ans, reply_markup=kb)
    await Admin.start.set()


@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.start)
async def admin_chat(call: types.CallbackQuery):
    chat = get_chat(1)
    option = get_sub_option(1)
    ans = '<u>Канал</u>\n' \
        '➖Название: <b>{}</b>\n' \
        '➖ID: <b>{}</b>\n' \
        '<u>Подписка</u>\n' \
        '➖Стоимость: <b>{} руб.</b>\n' \
        '➖Срок: <b>{} дней</b>'.format(
        chat[1], str(chat[2]).replace('-100', ''),
        option[1], option[2]
    )
    kb = await kb_admin()
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'exit', state=Admin.start)
async def admin_chat(call: types.CallbackQuery, state: FSMContext):
    ans = 'Вы вышли из панели редактирования'
    await call.message.edit_text(ans)
    await state.finish()


@dp.callback_query_handler(lambda c: c.data == 'chat', state=Admin.start)
async def admin_chat(call: types.CallbackQuery):
    chat = get_chat(1)
    ans = '<u>Канал</u>\n' \
        '➖Название: <b>{}</b>\n' \
        '➖ID: <b>{}</b>\n'.format(
        chat[1], str(chat[2]).replace('-100', '')
    )
    kb = await kb_edit_chat()
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'option', state=Admin.start)
async def admin_option(call: types.CallbackQuery):
    option = get_sub_option(1)
    ans = '<u>Подписка</u>\n' \
        '➖Стоимость: <b>{} руб.</b>\n' \
        '➖Срок: <b>{} дней</b>'.format(
        option[1], option[2]
    )
    kb = await kb_edit_option()
    await call.message.edit_text(ans, reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'edit_chat_name', state=Admin.start)
async def admin_option(call: types.CallbackQuery):
    ans = 'Введите новое название канала'
    await call.message.edit_text(ans)
    await Admin.edit_chat_name.set()


@dp.callback_query_handler(lambda c: c.data == 'edit_chat_id', state=Admin.start)
async def admin_option(call: types.CallbackQuery):
    ans = 'Введите новый ID канала'
    await call.message.edit_text(ans)
    await Admin.edit_chat_id.set()


@dp.callback_query_handler(lambda c: c.data == 'edit_option_price', state=Admin.start)
async def admin_option(call: types.CallbackQuery):
    ans = 'Введите новую цену подписки'
    await call.message.edit_text(ans)
    await Admin.edit_option_price.set()


@dp.callback_query_handler(lambda c: c.data == 'edit_option_time', state=Admin.start)
async def admin_option(call: types.CallbackQuery):
    ans = 'Введите новый срок подписки'
    await call.message.edit_text(ans)
    await Admin.edit_option_time.set()


@dp.message_handler(state=Admin.edit_chat_name)
async def cmd_admin(message: types.Message, state: FSMContext):
    chat = edit_chat_name(1, message.text)
    ans = 'Название: <b>{}</b>\nID: <b>{}</b>'.format(chat[1], str(chat[2]).replace('-100', ''))
    kb = await kb_edit_chat()
    await message.answer(ans, reply_markup=kb)
    await Admin.start.set()


@dp.message_handler(lambda message: message.text.isdigit(), state=Admin.edit_chat_id)
async def cmd_admin(message: types.Message, state: FSMContext):
    chat = edit_chat_id(1, int(message.text))
    ans = 'Название: <b>{}</b>\nID: <b>{}</b>'.format(chat[1], str(chat[2]).replace('-100', ''))
    kb = await kb_edit_chat()
    await message.answer(ans, reply_markup=kb)
    await Admin.start.set()


@dp.message_handler(lambda message: message.text.isdigit(), state=Admin.edit_option_price)
async def cmd_admin(message: types.Message, state: FSMContext):
    option = edit_sub_option_price(1, int(message.text))
    ans = 'Стоимость: <b>{} руб.</b>\nСрок: <b>{} дней</b>'.format(option[1], option[2])
    kb = await kb_edit_option()
    await message.answer(ans, reply_markup=kb)
    await Admin.start.set()


@dp.message_handler(lambda message: message.text.isdigit(), state=Admin.edit_option_time)
async def cmd_admin(message: types.Message, state: FSMContext):
    option = edit_sub_option_time(1, int(message.text))
    ans = 'Стоимость: <b>{} руб.</b>\nСрок: <b>{} дней</b>'.format(option[1], option[2])
    kb = await kb_edit_option()
    await message.answer(ans, reply_markup=kb)
    await Admin.start.set()