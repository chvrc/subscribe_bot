from db.chats import add_chat
from db.create_db import create_db
from db.sub_options import add_sub_option
from set_bot_commands import set_default_commands
from aiogram.utils import executor
from load_bot import bot, loop


async def on_shutdown(dp):
    await bot.close()


async def on_startup(dispatcher):
    await set_default_commands(dispatcher)
    create_db()


if __name__ == "__main__":
    from admin import dp
    from menu import dp
    from payments import dp
    executor.start_polling(dp, loop=loop,on_shutdown=on_shutdown, on_startup=on_startup)
