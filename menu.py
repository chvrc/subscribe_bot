from aiogram.dispatcher.filters.builtin import CommandStart
from aiogram.dispatcher.storage import FSMContext
from aiogram.dispatcher.filters import Command
from keyboards import kb_back, kb_cancel_sub, kb_desc, kb_email
from load_bot import dp
from aiogram import types
from messages import MESSAGE
from states import MainMenu
import re

 
def check_email(email):
    regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
    if(re.fullmatch(regex, email)):
        return True
    else:
        False


@dp.message_handler(CommandStart(), state='*')
async def cmd_start(message: types.Message):
    kb = await kb_desc()
    await message.answer(MESSAGE['start'], reply_markup=kb)
    await MainMenu.start.set()


@dp.callback_query_handler(lambda c: c.data == 'back', state=MainMenu.desc)
async def cmd_start(call: types.CallbackQuery, state: FSMContext):
    kb = await kb_desc()
    await call.message.edit_text(MESSAGE['start'], reply_markup=kb)
    await MainMenu.start.set()


@dp.callback_query_handler(lambda c: c.data == 'email', state=MainMenu.start)
async def pay_email(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_text(MESSAGE['email'])
    await MainMenu.email.set()


@dp.callback_query_handler(lambda c: c.data == 'desc', state=MainMenu.start)
async def cmd_start(call: types.CallbackQuery):
    kb = await kb_back()
    await call.message.edit_text(MESSAGE['desc'], reply_markup=kb)
    await MainMenu.desc.set()


@dp.message_handler(Command('sub'), state='*')
async def sub_settings(message: types.Message, state: FSMContext):

    kb = await kb_cancel_sub()

    await message.answer("<b>Подписка активна</b>\nСледующие 5000 руб. спишутся 20.02.2022\n<i>Для отмены подписки нажмите кнопку ниже</i>", reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'cancel_sub', state='*')
async def cancel_sub(call: types.CallbackQuery, state: FSMContext):

    await call.message.edit_reply_markup()

    await call.message.answer("Подписка отменена")
