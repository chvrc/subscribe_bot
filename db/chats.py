import sqlite3


def add_chat(name, chat_id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO chats(name, chat_id) VALUES(?, ?)
                """, (name, int(f'-100{chat_id}')))
    cur.execute("""
                SELECT * FROM chats
                ORDER BY id DESC LIMIT 1
                """)
    res = cur.fetchone()
    conn.commit()
    return res


def edit_chat_name(id, name):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE chats
                SET name = ?
                WHERE id = ?
                """, (name, id))
    cur.execute("""
                SELECT * FROM chats
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def edit_chat_id(id, chat_id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE chats
                SET chat_id = ?
                WHERE id = ?
                """, (int(f'-100{chat_id}'), id))
    cur.execute("""
                SELECT * FROM chats
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def del_chat(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM chats
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    cur.execute("""
                DELETE from chats
                WHERE id = ?;
                """, (id, ))
    conn.commit()
    return res


def get_chat(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM chats
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def get_chats():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM chats
                """)
    res = cur.fetchall()
    conn.commit()
    return res
