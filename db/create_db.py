import sqlite3

def create_db():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""CREATE TABLE IF NOT EXISTS chats(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    name INTEGER,
                    chat_id INTEGER UNIQUE)
                    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS subs(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    chat INTEGER,
                    sub_uid INTEGER UNIQUE,
                    sub_time INTEGER)
                    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS sub_options(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    price INTEGER UNIQUE,
                    time INTEGER UNIQUE)
                    """)
    conn.commit()