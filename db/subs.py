import sqlite3
import time
from datetime import datetime


def add_sub(uid, sub_time):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO subs(uid, sub_time) VALUES(?, ?)
                """, (uid, sub_time))
    cur.execute("""
                SELECT * FROM subs
                ORDER BY id DESC LIMIT 1
                """)
    res = cur.fetchone()
    conn.commit()
    return res


def edit_sub_time(uid, sub_time):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE subs
                SET sub_time = ?
                WHERE uid = ?
                """, (sub_time, uid))
    cur.execute("""
                SELECT * FROM subs
                WHERE uid = ?
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def del_sub(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM subs
                WHERE uid = ?
                """, (id, ))
    res = cur.fetchone()
    cur.execute("""
                DELETE from subs
                WHERE uid=?;
                """, (uid, ))
    conn.commit()
    return res


def get_sub(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM subs
                WHERE id = ?
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return res


def get_subs():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM subs
                """)
    res = cur.fetchall()
    conn.commit()
    return res