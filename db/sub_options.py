import sqlite3
import time
from datetime import datetime


def add_sub_option(price, sub_time):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO sub_options(price, time) VALUES(?, ?)
                """, (price, sub_time))
    cur.execute("""
                SELECT * FROM sub_options
                ORDER BY id DESC LIMIT 1
                """)
    res = cur.fetchone()
    conn.commit()
    return res


def edit_sub_option_price(id, price):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE sub_options
                SET price = ?
                WHERE id = ?
                """, (price, id))
    cur.execute("""
                SELECT * FROM sub_options
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def edit_sub_option_time(id, sub_time):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE sub_options
                SET time = ?
                WHERE id = ?
                """, (sub_time, id))
    cur.execute("""
                SELECT * FROM sub_options
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def del_sub_option(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM sub_options
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    cur.execute("""
                DELETE from sub_options
                WHERE id = ?;
                """, (id, ))
    conn.commit()
    return res


def get_sub_option(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM sub_options
                WHERE id = ?
                """, (id, ))
    res = cur.fetchone()
    conn.commit()
    return res


def get_sub_options():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM sub_options
                """)
    res = cur.fetchall()
    conn.commit()
    return res