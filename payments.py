from aiogram.dispatcher.storage import FSMContext
from db.chats import get_chat
from db.sub_options import get_sub_option
from keyboards import kb_pay_yookassa, kb_success_pay
from load_bot import dp, bot
from aiogram import types
from menu import check_email
from messages import MESSAGE, TERMS_OF_USE
from states import MainMenu
import asyncio

from yookassa import Payment
from yookassa.domain.models.currency import Currency
from yookassa.domain.models.receipt import Receipt
from yookassa.domain.models.receipt_item import PaymentMode, PaymentSubject, ReceiptItem
from yookassa.domain.common.confirmation_type import ConfirmationType
from yookassa.domain.request.payment_request_builder import PaymentRequestBuilder


def create_payment(email, user_id):

    chat = get_chat(1)
    option = get_sub_option(1)

    description = 'Подписка на {} дней для канала {}'.format(option[2], chat[1])
    price = float(option[1])

    receipt = Receipt()
    receipt.customer = {"email": email}
    receipt.items = [
        ReceiptItem({
            "description": description,
            "quantity": 1.0,
            "amount": {
                "value": price,
                "currency": Currency.RUB
            },
            "vat_code": 1,
            "payment_mode": PaymentMode.FULL_PREPAYMENT,
            "payment_subject": PaymentSubject.SERVICE
        })
    ]

    builder = PaymentRequestBuilder()
    builder.set_amount({"value": price, "currency": Currency.RUB}) \
        .set_confirmation({"type": ConfirmationType.REDIRECT, "return_url": "https://t.me/sneakerclub_bot"}) \
        .set_capture(True) \
        .set_description(description) \
        .set_metadata({"user_id": f"{user_id}"}) \
        .set_receipt(receipt)
        
    request = builder.build()
    res = Payment.create(request)
    return res.id, res.confirmation.confirmation_url


async def check_payment(payment_id):
    while True:
        payment = Payment.find_one(payment_id)
        if payment.status == 'succeeded':
            user_id = int(payment.metadata['user_id'])
            chat = get_chat(1)
            option = get_sub_option(1)
            member_count = await bot.get_chat_members_count(chat[2])
            invite_link = await bot.create_chat_invite_link(chat_id=chat[2], member_limit=1)

            await dp.bot.unban_chat_member(chat_id=chat[2], user_id=user_id, only_if_banned=True)

            kb = await kb_success_pay(invite_link['invite_link'])
            await bot.send_message(user_id,
                                    MESSAGE['success'].format(option[2], chat[1], member_count),
                                    reply_markup=kb)
            break
        elif payment.status == 'canceled':
            user_id = int(payment.metadata['user_id'])
            await bot.send_message(user_id, 'Что-то пошло не так. Платёж не прошёл.')
            break
        await asyncio.sleep(15)


@dp.message_handler(lambda message: check_email(message.text), state=MainMenu.email)
async def get_valid_email(message: types.Message, state: FSMContext):

    email = message.text

    async with state.proxy() as data:
        data['email'] = email

    user_id = message.from_user.id
    chat = get_chat(1)
    option = get_sub_option(1)
    member_count = await bot.get_chat_members_count(chat[2])

    payment = create_payment(email, user_id)
    payment_id = payment[0]
    payment_url = payment[1]
    kb = await kb_pay_yookassa(payment_url)
    ans = MESSAGE['pay'].format(option[2], chat[1], member_count, option[1], TERMS_OF_USE)
    await message.answer(ans, reply_markup=kb,
                        disable_web_page_preview=True)
    await state.finish()
    await check_payment(payment_id)


@dp.message_handler(state=MainMenu.email)
async def error_invalid_email(message: types.Message):
    
    await message.answer(MESSAGE['invalid_email'])
